# This file is part of Misaka.
#
# Misaka is free software: you can redistribute it and/or modify it
# under the terms of the GNU Affero General Public License as published
# by the Free Software Foundation, version 3.
#
# Misaka is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
# See the GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with Misaka. If not, see <https://www.gnu.org/licenses/>.

CFLAGS += -Iinclude -Ideps/Index/include
CFLAGS += -O2 -DNDEBUG -std=gnu99
CFLAGS += -Wall -Wextra -Wpedantic
CFLAGS += -Wno-unused-function -Wno-unused-parameter

all: build/misaka

install:
	cp build/misaka "$(DESTDIR)/usr/bin/"
uninstall:
	rm -f "$(DESTDIR)/usr/bin/misaka"

clean:
	rm -rf build
depclean:
	rm -rf deps

build:
	mkdir build
deps:
	mkdir deps

deps/Index/include/structure/vector.def:
deps/Index/include/structure/mapping.def: | deps
	cd deps && git clone https://gitlab.com/BasedRimuru/Index

build/misaka: include/misaka/common.h include/misaka/interface.h \
              src/common.c src/interface/host.c src/interface/connect.c \
              deps/Index/include/structure/vector.def \
              deps/Index/include/structure/mapping.def \
              main.c | build
	$(CC) $(CFLAGS) src/common.c src/interface/* main.c -o $@
