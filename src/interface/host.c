/*
This file is part of Misaka.

Misaka is free software: you can redistribute it and/or modify it
under the terms of the GNU Affero General Public License as published
by the Free Software Foundation, version 3.

Misaka is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
See the GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with Misaka. If not, see <https://www.gnu.org/licenses/>.
*/

#include <errno.h>
#include <stdio.h>
#include <signal.h>
#include <stdlib.h>
#include <string.h>

#include <fcntl.h>
#include <unistd.h>
#include <sys/un.h>
#include <sys/time.h>
#include <sys/types.h>
#include <sys/socket.h>

#include "misaka/common.h"

/* Type definitions */
/* Creates and includes the necessary types */

struct client
{
    int socket;
    struct channel *channel;
    bool transmitter;
};

struct channel
{
    struct char_vec_t *name;
    struct client_map_t *connections;
};

struct server
{
    struct sockaddr_un address;
    int socket;

    struct char_vec_t *buffer;

    struct client_vec_t *clients;
    struct channel_map_t *channels;

    int stopper[2];
};

static size_t
client_map_hash(struct client *cl)
{
    size_t ret = 0;

    if (cl != NULL)
        ret = cl->socket;

    return ret;
}

static bool
client_map_test(struct client *cl, struct client *cl2)
{
    bool ret = false;

    if (cl != NULL && cl2 != NULL)
        ret = (cl->socket == cl2->socket);

    return ret;
}

static size_t
channel_map_hash(struct channel *ch)
{
    size_t ret = 5381;

    if (ch != NULL && ch->name != NULL)
    {
        for (size_t i = 0; i < ch->name->count; i++)
            ret = ((ret << 5) + ret) + ch->name->data[0];
    }

    return ret;
}

static bool
channel_map_test(struct channel *ch, struct channel *ch2)
{
    bool ret = false;

    if (ch != NULL && ch2 != NULL && ch->name != NULL && ch2->name != NULL)
    {
        if (ch->name->count == ch2->name->count)
        {
            if (memcmp(ch->name->data, ch2->name->data, ch->name->count) == 0)
                ret = true;
        }
    }

    return ret;
}

#define X_TYPE struct client
#define X_NAMING(x) client_vec_##x
#include "structure/vector.def"
#undef X_NAMING
#define X_NAMING(x) client_map_##x
#include "structure/mapping.def"
#undef X_NAMING
#undef X_TYPE

#define X_TYPE struct channel
#define X_NAMING(x) channel_map_##x
#include "structure/mapping.def"
#undef X_TYPE
#undef X_NAMING

/* Parsing functions */
/* Executes the received commands */

static enum status
parse_message(struct server *sv, struct client *cl)
{
    enum status ret = STATUS_OK;

    if (cl->channel != NULL)
    {
        if (cl->transmitter && cl->channel->connections != NULL)
        {
            for (size_t i = 0; i < cl->channel->connections->size; i++)
            {
                if (cl->channel->connections->meta[i] <= 0x7f &&
                    !(cl->channel->connections->data[i].transmitter))
                {
                    message_send(cl->channel->connections->data[i].socket,
                                 sv->buffer);
                }
            }
        }
        else
        {
            ret = STATUS_HALT;
        }
    }
    else
    {
        if (sv->buffer->data[0] == '>')
        {
            cl->transmitter = true;
        }
        else if (sv->buffer->data[0] != '<')
        {
            ret = STATUS_HALT;
        }
        char_vec_delete(sv->buffer, &(sv->buffer->data[0]));

        if (ret == STATUS_OK)
        {
            struct channel query2 = {.name = sv->buffer};
            cl->channel = channel_map_search(sv->channels, &query2);
            if (cl->channel != NULL)
            {
                if (!client_map_insert(cl->channel->connections, cl))
                    ret = status_log("No memory", STATUS_FATAL);
            }
            else
            {
                struct channel new = {0};
                new.name = char_vec_copy(sv->buffer);
                new.connections = client_map_alloc(32);
                if (new.name != NULL && new.connections != NULL)
                {
                    if (!channel_map_insert(sv->channels, &new))
                        ret = status_log("No memory", STATUS_FATAL);

                    if (ret == STATUS_OK)
                    {
                        cl->channel = channel_map_search(sv->channels, &new);
                        if (cl->channel == NULL)
                            ret = status_log("No memory", STATUS_FATAL);
                    }

                    if (ret == STATUS_OK)
                    {
                        if (!client_map_insert(cl->channel->connections, cl))
                            ret = status_log("No memory", STATUS_FATAL);
                    }
                }
                else
                {
                    char_vec_free(new.name);
                    client_map_free(new.connections);
                    ret = status_log("No memory", STATUS_FATAL);
                }
            }
        }
    }

    return ret;
}

/* Connection functions */
/* Gets the commands and attach new sockets */

static enum status
connection_master(struct server *sv)
{
    enum status ret = STATUS_OK;

    int addr_s = sizeof(struct sockaddr_un);
    int socket = accept(sv->socket, (struct sockaddr *)&(sv->address),
                        (socklen_t *)&addr_s);
    if (socket < 0)
        ret = status_log("A client failed to connect", STATUS_ERROR);

    if (ret == STATUS_OK)
    {
        bool success = false;
        for (u8 i = 0; i < sv->clients->count; i++)
        {
            if (sv->clients->data[i].socket == 0)
            {
                sv->clients->data[i].socket = socket;
                success = true;
                break;
            }
        }

        if (!success)
        {
            struct client dummy = {.socket = socket, .channel = NULL};
            if (!client_vec_append(sv->clients, &dummy))
                ret = status_log("No memory", STATUS_FATAL);
        }
    }

    if (ret == STATUS_OK)
        status_log("Client connected", ret);

    if (ret < STATUS_FATAL)
        ret = STATUS_OK;

    return ret;
}

static enum status
connection_slave(struct server *sv, struct client *cl)
{
    enum status ret = STATUS_OK;

    u32 length = 0;
    int bytes = read(cl->socket, &length, 4);
    if (bytes <= 0)
    {
        ret = status_log("Connection ended", STATUS_HALT);
    }
    else if (bytes != 4)
    {
        ret = status_log("Broken connection", STATUS_ERROR);
    }

    if (ret == STATUS_OK)
    {
        if (length != 0)
        {
            char dummy = '\0';
            if (sv->buffer->count < length)
            {
                if (!char_vec_insert(sv->buffer, &dummy, length - 1))
                    ret = status_log("No memory", STATUS_FATAL);
            }

            sv->buffer->count = length;
            if (ret == STATUS_OK)
            {
                if (read(cl->socket, sv->buffer->data, length) != length)
                    ret = status_log("Broken connection", STATUS_ERROR);
            }

            if (ret == STATUS_OK)
                ret = parse_message(sv, cl);
        }
    }

    if (ret != STATUS_OK)
    {
        close(cl->socket);

        if (cl->channel != NULL)
        {
            if (cl->channel->connections == NULL ||
                cl->channel->connections->count < 2)
            {
                char_vec_free(cl->channel->name);
                client_map_free(cl->channel->connections);
                channel_map_delete(sv->channels, cl->channel);
            }
            else
            {
                struct client *ptr = NULL;
                ptr = client_map_search(cl->channel->connections, cl);

                client_map_delete(cl->channel->connections, ptr);
            }
        }

        client_vec_delete(sv->clients, cl);
    }

    if (ret < STATUS_FATAL)
        ret = STATUS_OK;

    return ret;
}

/* Server functions */
/* Controls the server functionality */

static void
server_clean(struct server *sv)
{
    if (sv != NULL)
    {
        if (sv->socket > 0)
            close(sv->socket);

        if (sv->channels != NULL)
        {
            for (size_t i = 0; i < sv->channels->size; i++)
            {
                if (sv->channels->meta[i] <= 0x7f)
                {
                    char_vec_free(sv->channels->data[i].name);
                    client_map_free(sv->channels->data[i].connections);
                }
            }
        }

        char_vec_free(sv->buffer);
        client_vec_free(sv->clients);
        channel_map_free(sv->channels);

        if (sv->address.sun_path[0] != '\0')
            unlink(sv->address.sun_path);
    }
}

static enum status
server_init(struct server *sv, char *path)
{
    enum status ret = STATUS_OK;

    sv->address.sun_family = AF_UNIX;
    strcpy(sv->address.sun_path, path);

    if ((sv->socket = socket(AF_UNIX, SOCK_STREAM, 0)) < 0)
        ret = status_log("Failed to create socket", STATUS_ERROR);

    if (ret == STATUS_OK)
    {
        if (bind(sv->socket, (struct sockaddr *)&sv->address,
                 sizeof(struct sockaddr_un)) < 0)
        {
            ret = status_log("Failed to create socket", STATUS_ERROR);
        }
    }

    if (ret == STATUS_OK)
    {
        if (listen(sv->socket, 128) < 0)
            ret = status_log("Failed to create socket", STATUS_ERROR);
    }

    if (ret == STATUS_OK)
    {
        sv->buffer = char_vec_alloc(1024);
        sv->clients = client_vec_alloc(128);
        sv->channels = channel_map_alloc(128);
        if (sv->buffer == NULL || sv->clients == NULL || sv->channels == NULL)
            ret = status_log("No memory", STATUS_FATAL);
    }

    if (ret != STATUS_OK)
    {
        sv->address.sun_path[0] = '\0';
        server_clean(sv);
    }

    return ret;
}

static enum status
server_run(struct server *sv)
{
    enum status ret = STATUS_OK;

    size_t robin = 0;
    while (ret == STATUS_OK)
    {
        fd_set fd_arr = {0};

        FD_ZERO(&fd_arr);
        FD_SET(sv->socket, &fd_arr);
        FD_SET(sv->stopper[0], &fd_arr);

        int max = (sv->socket > sv->stopper[0]) ? sv->socket : sv->stopper[0];
        for (u8 i = 0; i < sv->clients->count; i++)
        {
            int client = sv->clients->data[i].socket;
            if (client > 0)
                FD_SET(client, &fd_arr);

            if (client > max)
                max = client;
        }

        int result = select(max + 1, &fd_arr, NULL, NULL, NULL);
        if (result < 0 && errno != EINTR && errno != EAGAIN)
            ret = status_log("Failed select", STATUS_ERROR);

        if (ret == STATUS_OK && FD_ISSET(sv->stopper[0], &fd_arr))
            ret = STATUS_HALT;

        if (ret == STATUS_OK && FD_ISSET(sv->socket, &fd_arr))
            ret = connection_master(sv);

        if (ret == STATUS_OK)
        {
            for (size_t i = 0; i < sv->clients->count; i++)
            {
                size_t j = (i + robin) % sv->clients->count;
                if (FD_ISSET(sv->clients->data[j].socket, &fd_arr))
                    ret = connection_slave(sv, &(sv->clients->data[j]));
            }

            if (sv->clients->count > 0)
            {
                robin = (robin + 1) % sv->clients->count;
            }
            else
            {
                robin = 0;
            }
        }
    }

    return ret;
}

/* Interface functions */
/* Exports functionality to the user */

static int stopper_w;
static void
handler(int code)
{
    switch (code)
    {
        case SIGINT:
        case SIGTERM:
            if (stopper_w > 0)
            {
                char dummy = '\0';
                write(stopper_w, &dummy, 1);
            }
            break;

        default:
            break;
    }
}

static enum status
usage(void)
{
    fprintf(stderr, "usage: misaka host SOCKET\n");
    fprintf(stderr, "Hosts the network, acting like a router\n");
    return STATUS_ERROR;
}

extern int
misaka_host(int argc, char *argv[])
{
    enum status ret = STATUS_OK;

    if (argc == 2)
    {
        struct server sv = {0};

        ret = server_init(&sv, argv[1]);

        if (ret == STATUS_OK)
        {
            if (pipe(sv.stopper) < 0)
                ret = status_log("Pipe failed", STATUS_FATAL);
        }

        if (ret == STATUS_OK)
        {
            fcntl(sv.stopper[0], F_SETFL, O_NONBLOCK);
            fcntl(sv.stopper[1], F_SETFL, O_NONBLOCK);
            stopper_w = sv.stopper[1];
            signal(SIGINT, handler);
            signal(SIGTERM, handler);
            signal(SIGPIPE, handler);
        }

        if (ret == STATUS_OK)
            ret = server_run(&sv);

        server_clean(&sv);
    }
    else
    {
        ret = usage();
    }

    return ret > STATUS_HALT;
}
