/*
This file is part of Misaka.

Misaka is free software: you can redistribute it and/or modify it
under the terms of the GNU Affero General Public License as published
by the Free Software Foundation, version 3.

Misaka is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
See the GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with Misaka. If not, see <https://www.gnu.org/licenses/>.
*/

#include <errno.h>
#include <stdio.h>
#include <signal.h>
#include <stdlib.h>
#include <string.h>

#include <fcntl.h>
#include <unistd.h>
#include <sys/un.h>
#include <sys/socket.h>

#include "misaka/common.h"

/* Type definitions */
/* struct client is the only different type here */

struct client
{
    char *address;
    char *topic;

    int socket;
    struct char_vec_t *buffer;

    int stopper[2];
};

/* Connection functions */
/* Talks to the server and handles IO */

static enum status
identify(struct client *cl, bool transmit)
{
    enum status ret = STATUS_OK;

    cl->socket = 0;
    if ((cl->socket = socket(AF_UNIX, SOCK_STREAM, 0)) < 0)
        ret = status_log("Failed to connect", STATUS_ERROR);

    struct sockaddr_un addr = {0};
    if (ret == STATUS_OK)
    {
        addr.sun_family = AF_UNIX;
        strcpy(addr.sun_path, cl->address);

        if (connect(cl->socket, (struct sockaddr *)&addr, sizeof(addr)) < 0)
            ret = status_log("Failed to connect", STATUS_ERROR);
    }

    if (ret == STATUS_OK)
    {
        struct char_vec_t *header = char_vec_alloc(32);
        if (header != NULL)
        {
            char side = (transmit) ? '>' : '<';
            if (!char_vec_append(header, &side))
                ret = status_log("No memory", STATUS_FATAL);

            for (size_t i = 0; cl->topic[i] != '\0'; i++)
            {
                if (!char_vec_append(header, &(cl->topic[i])))
                {
                    ret = status_log("No memory", STATUS_FATAL);
                    break;
                }
            }

            if (message_send(cl->socket, header) != STATUS_OK)
                ret = status_log("Failed to connect", STATUS_ERROR);
        }
        else
        {
            ret = status_log("No memory", STATUS_FATAL);
        }
        char_vec_free(header);
    }

    return ret;
}

static enum status
transmitter(struct client *cl)
{
    enum status ret = STATUS_OK;

    while (ret == STATUS_OK)
    {
        fd_set fd_arr = {0};

        FD_ZERO(&fd_arr);
        FD_SET(STDIN_FILENO, &fd_arr);
        FD_SET(cl->stopper[0], &fd_arr);

        int max = (STDIN_FILENO > cl->stopper[0]) ? STDIN_FILENO :
                                                    cl->stopper[0];
        int result = select(max + 1, &fd_arr, NULL, NULL, NULL);
        if (result < 0 && errno != EINTR && errno != EAGAIN)
            ret = status_log("Failed select", STATUS_ERROR);

        if (ret == STATUS_OK && FD_ISSET(cl->stopper[0], &fd_arr))
            ret = STATUS_HALT;

        if (ret == STATUS_OK && FD_ISSET(STDIN_FILENO, &fd_arr))
        {
            int c = fgetc(stdin);
            if (c == EOF)
                break;
            ungetc(c, stdin);

            cl->buffer->count = 0;

            c = '?';
            while (c != '\n' && c != EOF)
            {
                c = fgetc(stdin);

                char cc = c;
                if (!char_vec_append(cl->buffer, &cc))
                    ret = status_log("No memory", STATUS_FATAL);
            }

            if (ret == STATUS_OK && (cl->buffer->count > 1 || c != EOF))
                ret = message_send(cl->socket, cl->buffer);
        }
    }

    return ret;
}

static enum status
receiver(struct client *cl)
{
    enum status ret = STATUS_OK;

    while (ret == STATUS_OK)
    {
        fd_set fd_arr = {0};

        FD_ZERO(&fd_arr);
        FD_SET(cl->socket, &fd_arr);
        FD_SET(cl->stopper[0], &fd_arr);

        int max = (cl->socket > cl->stopper[0]) ? cl->socket : cl->stopper[0];
        int result = select(max + 1, &fd_arr, NULL, NULL, NULL);
        if (result < 0 && errno != EINTR && errno != EAGAIN)
            ret = status_log("Failed select", STATUS_ERROR);

        if (ret == STATUS_OK && FD_ISSET(cl->stopper[0], &fd_arr))
            ret = STATUS_HALT;

        if (ret == STATUS_OK && FD_ISSET(cl->socket, &fd_arr))
        {
            u32 dummy = 0;
            if (recv(cl->socket, &dummy, 4, MSG_PEEK) != 4)
                ret = STATUS_HALT;

            if (ret == STATUS_OK)
                ret = message_recv(cl->socket, cl->buffer);

            if (ret == STATUS_OK)
            {
                write(STDOUT_FILENO, cl->buffer->data, cl->buffer->count);
                fsync(STDOUT_FILENO);
            }
        }
    }

    return ret;
}

/* Interface functions */
/* Exports functionality to the user */

static int stopper_w;
static void
handler(int code)
{
    if (stopper_w > 0)
    {
        char dummy = '\0';
        write(stopper_w, &dummy, 1);
    }
}

static enum status
usage(void)
{
    fprintf(stderr, "usage: misaka connect SOCKET CHANNEL xmtr/rcvr\n");
    fprintf(stderr,
            "Connects to the network, transmitting or receiving data\n");
    return STATUS_ERROR;
}

extern int
misaka_connect(int argc, char *argv[])
{
    enum status ret = STATUS_OK;

    if (argc == 4)
    {
        struct client cl = {0};
        cl.address = argv[1];
        cl.topic = argv[2];

        bool transmit = false;
        if (strcmp(argv[3], "xmtr") == 0)
        {
            transmit = true;
        }
        else if (strcmp(argv[3], "rcvr") != 0)
        {
            ret = usage();
        }

        if (ret == STATUS_OK)
        {
            if (pipe(cl.stopper) < 0)
                ret = status_log("Pipe failed", STATUS_FATAL);
        }

        if (ret == STATUS_OK)
        {
            fcntl(cl.stopper[0], F_SETFL, O_NONBLOCK);
            fcntl(cl.stopper[1], F_SETFL, O_NONBLOCK);
            stopper_w = cl.stopper[1];
            signal(SIGINT, handler);
            signal(SIGTERM, handler);
            signal(SIGPIPE, handler);

            cl.buffer = char_vec_alloc(128);
            if (cl.buffer == NULL)
                ret = status_log("No memory", STATUS_FATAL);
        }

        if (ret == STATUS_OK)
            ret = identify(&cl, transmit);

        if (ret == STATUS_OK)
        {
            if (transmit)
            {
                ret = transmitter(&cl);
            }
            else
            {
                ret = receiver(&cl);
            }
        }

        if (cl.socket > 0)
            close(cl.socket);
        char_vec_free(cl.buffer);
        memset(&cl, 0, sizeof(struct client));
    }
    else
    {
        ret = usage();
    }

    return ret > STATUS_HALT;
}
