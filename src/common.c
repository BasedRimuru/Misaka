/*
This file is part of Misaka.

Misaka is free software: you can redistribute it and/or modify it
under the terms of the GNU Affero General Public License as published
by the Free Software Foundation, version 3.

Misaka is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
See the GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with Misaka. If not, see <https://www.gnu.org/licenses/>.
*/

#include <stdio.h>
#include <stdlib.h>

#include <unistd.h>
#include <sys/socket.h>

#include "misaka/common.h"

extern enum status
status_log(char *message, enum status st)
{
    switch (st)
    {
        case STATUS_ERROR:
            fprintf(stderr, "misaka: error: %s\n", message);
            break;
        case STATUS_FATAL:
            fprintf(stderr, "misaka: fatal: %s\n", message);
            break;
        default:
            #ifndef NDEBUG
            fprintf(stderr, "misaka: debug: %s\n", message);
            #endif
            break;
    }

    return st;
}

extern enum status
message_send(int socket, struct char_vec_t *buffer)
{
    enum status ret = STATUS_OK;

    if (buffer != NULL)
    {
        u32 length = buffer->count;
        if (send(socket, &length, 4, MSG_NOSIGNAL) != 4)
            ret = STATUS_ERROR;

        if (ret == STATUS_OK && buffer->count != 0)
        {
            if ((unsigned)send(socket, buffer->data, buffer->count, 0)
                != buffer->count)
            {
                ret = STATUS_ERROR;
            }
        }
    }
    else
    {
        ret = STATUS_ERROR;
    }

    return ret;
}

extern enum status
message_recv(int socket, struct char_vec_t *buffer)
{
    enum status ret = STATUS_OK;

    if (buffer != NULL)
    {
        u32 length = 0;
        if (recv(socket, &length, 4, 0) != 4)
            ret = STATUS_ERROR;

        if (ret == STATUS_OK && length != 0)
        {
            if (buffer->size < length)
            {
                char dummy = '\0';
                char_vec_insert(buffer, &dummy, length - 1);
            }

            if (ret == STATUS_OK)
            {
                if (recv(socket, buffer->data, length, 0) != length)
                    ret = STATUS_ERROR;
            }
        }
        buffer->count = (ret == STATUS_OK) ? length : 0;
    }
    else
    {
        ret = STATUS_ERROR;
    }

    return ret;
}
