# Misaka

An IPC network implementing the publish-subscribe pattern.

## Installation
Needs a C compiler, a POSIX system, and make.
```
make
sudo make install
```

## Example
Router
```sh
misaka host example.socket
``` 

Receiver 1
```sh
misaka connect example.socket mychannel rcvr
``` 

Receiver 2
```sh
misaka connect example.socket mychannel rcvr
``` 

Transmitter
```sh
echo "Testing! - says Misaka" | misaka connect example.socket mychannel xmtr
```
