/*
This file is part of Misaka.

Misaka is free software: you can redistribute it and/or modify it
under the terms of the GNU Affero General Public License as published
by the Free Software Foundation, version 3.

Misaka is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
See the GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with Misaka. If not, see <https://www.gnu.org/licenses/>.
*/

#include <stdio.h>
#include <string.h>

#include "misaka/interface.h"

static int
usage(void)
{
    fprintf(stderr, "usage: misaka host/connect\n");
    fprintf(stderr, "Inter-process network\n");
    return 1;
}

extern int
main(int argc, char *argv[])
{
    int ret = 0;

    if (argc >= 2)
    {
        if (strcmp(argv[1], "host") == 0)
        {
            ret = misaka_host(argc - 1, &(argv[1]));
        }
        else if (strcmp(argv[1], "connect") == 0)
        {
            ret = misaka_connect(argc - 1, &(argv[1]));
        }
        else
        {
            ret = usage();
        }
    }
    else
    {
        ret = usage();
    }

    return ret;
}
