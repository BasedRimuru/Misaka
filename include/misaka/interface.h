/*
This file is part of Misaka.

Misaka is free software: you can redistribute it and/or modify it
under the terms of the GNU Affero General Public License as published
by the Free Software Foundation, version 3.

Misaka is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
See the GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with Misaka. If not, see <https://www.gnu.org/licenses/>.
*/

#ifndef INTERFACE_H
#define INTERFACE_H

int misaka_host(int argc, char *argv[]);
int misaka_connect(int argc, char *argv[]);

#endif
