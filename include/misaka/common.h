/*
This file is part of Misaka.

Misaka is free software: you can redistribute it and/or modify it
under the terms of the GNU Affero General Public License as published
by the Free Software Foundation, version 3.

Misaka is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
See the GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with Misaka. If not, see <https://www.gnu.org/licenses/>.
*/

#ifndef COMMON_H
#define COMMON_H

#include <stdint.h>
#include <stdbool.h>

#define X_TYPE char
#define X_NAMING(x) char_vec_##x
#include "structure/vector.def"
#undef X_TYPE
#undef X_NAMING

typedef uint8_t u8;
typedef uint16_t u16;
typedef uint32_t u32;

enum status
{
    STATUS_OK,
    STATUS_HALT,
    STATUS_ERROR,
    STATUS_FATAL
};

enum status status_log(char *message, enum status st);

enum status message_send(int socket, struct char_vec_t *buffer);
enum status message_recv(int socket, struct char_vec_t *buffer);

#endif
